package com.phoenix.gmail;


import com.phoenix.gmail.models.Client;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.mail.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

public class App {
    static Folder emailFolder;
    static Store store;


    static String[] getInputs() {
        String[] inputs = new String[2];
        String username = "";

        System.out.println("Enter your email account :: ");
        Scanner scannerInput = new Scanner(System.in);
        username = scannerInput.nextLine();

        System.out.println("Enter password :: ");
        String password = scannerInput.nextLine();

        inputs[0] = username;
        inputs[1] = password;


        return inputs;
    }


    static Message[] connectAndGetMessages() throws MessagingException {
        String inputs[] = getInputs();

        String username = inputs[0];
        String password = inputs[1];

        Properties props = System.getProperties();
        props.setProperty("mail.store.protocol", "imaps");

        Session session = Session.getDefaultInstance(props, null);
        store = session.getStore("imaps");
        store.connect("imap.gmail.com", username, password);

        //create the folder object and open it
        emailFolder = store.getFolder("Inbox");

        emailFolder.open(Folder.READ_ONLY);

        // retrieve the messages from the folder in an array and print it
        //close the store and folder objects
        return emailFolder.getMessages();
    }

    public static void main(String[] args) {
        try {
            Message[] messages = connectAndGetMessages();

            int total = messages.length;
            String anim = "|/-\\";

            List<Client> clients = new ArrayList<>();

            for (int i = total - 1; i > 0; i--) {
                int iPlus1 = (i + 1);
                String data = "\r" + anim.charAt(iPlus1 % anim.length()) + " " + iPlus1 + " of " + total;
                System.out.write(data.getBytes());

                Message message = messages[i];
                Object content = message.getContent();


                if (message.getSubject() != null && message.getSubject().contains("Inquiry from website")) {
//                        System.out.println("---------------------------------");
//                        System.out.println("Email Number " + (i + 1));
//                        System.out.println("Received Date:" + message.getReceivedDate());
//                        System.out.println("Subject: " + message.getSubject());
//                        System.out.println("From: " + message.getFrom()[0]);
//                        System.out.println("To: " + message.getAllRecipients().toString());

                    String body;

                    if (content instanceof Multipart) {
                        Multipart multipart = (Multipart) content;
                        BodyPart bodyPart = multipart.getBodyPart(0);
                        body = bodyPart.getContent().toString();
                    } else {
                        body = content.toString();
                    }

                    String[] lines = body.split("\\r?\\n");

                    String name = "", email = "", phone = "";

                    for (String line : lines) {
                        if (line.startsWith("Names:")) {
                            name = line.replace("Names:", "").trim();
                        }

                        if (line.startsWith("Email:")) {
                            email = line.replace("Email:", "").trim();
                        }

                        if (line.startsWith("Phone:")) {
                            phone = line.replace("Phone:", "").trim();
                        }
                    }

                    clients.add(new Client(name, email, phone));
//                    System.out.println("\n" + name + "\n" + email + "\n" + phone);
                }
            }

            emailFolder.close(false);
            store.close();

            writeToExcel(clients);
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
            System.exit(1);
        } catch (MessagingException e) {
            e.printStackTrace();
            System.exit(2);
        } catch (IOException ioe) {
            ioe.printStackTrace();
            System.exit(5);
        }
    }


    static void writeToExcel(List<Client> clients) throws IOException {
        String[] columns = {"Name", "Email", "Phone"};

        // Create a Workbook
        Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file

        /* CreationHelper helps us create instances of various things like DataFormat,
           Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way */
        CreationHelper createHelper = workbook.getCreationHelper();

        // Create a Sheet
        Sheet sheet = workbook.createSheet("Client");

        // Create a Font for styling header cells
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.RED.getIndex());

        // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        // Create a Row
        Row headerRow = sheet.createRow(0);

        // Create cells
        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        // Create Cell Style for formatting Date
        CellStyle dateCellStyle = workbook.createCellStyle();
        dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));

        // Create Other rows and cells with clients data
        int rowNum = 1;
        for (Client employee : clients) {
            Row row = sheet.createRow(rowNum++);

            row.createCell(0)
                    .setCellValue(employee.getName());

            row.createCell(1)
                    .setCellValue(employee.getEmail());

            Cell dateOfBirthCell = row.createCell(2);
            dateOfBirthCell.setCellValue(employee.getPhone());
            dateOfBirthCell.setCellStyle(dateCellStyle);
        }

        // Resize all columns to fit the content size
        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        String fileName = (new Date()).getTime() + "-clients.xlsx";
        // Write the output to a file
        FileOutputStream fileOut = new FileOutputStream(fileName);
        workbook.write(fileOut);
        fileOut.close();

        // Closing the workbook
        workbook.close();

        System.out.println("Done writing to excel");
    }


}
